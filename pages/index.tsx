import * as React from "react";
import {fetchChannelsWithProgram} from "../redux/channelsProgram/actions";
import {withRedux} from "../redux/WithRedux";
import {ChannelProgram} from "../components/channelsProgramm";

const Home = () => {
    return <div>
        <div>
            Телепрограмма
        </div>
        <ChannelProgram/>
    </div>
}

Home.getInitialProps = async ({reduxStore}) => {
    const dateFrom = new Date()
    dateFrom.setHours(0, 0, 0)
    const dateTo = new Date()
    dateTo.setHours(23, 59, 59)
    await fetchChannelsWithProgram(reduxStore, {
        dateFrom,
        dateTo,
        domain: 'kazan'
    })
}

export default withRedux(Home)
