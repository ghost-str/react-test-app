import {DrawProgram} from "./DrowProgram";
import * as React from "react";
import styled from "styled-components";
import {ChannelStateInterface} from "../../redux/channelsProgram/reduser";

interface DrawChannelProps {
    channel: ChannelStateInterface
}

export function DrawChannel(props: DrawChannelProps) {
    return <div>
        <ChannelHeader>
            <ChannelIcon src={props.channel.logo}/>
            <div>
                {props.channel.title}
            </div>
        </ChannelHeader>
        <ProgramContainer>
            {props.channel.program.map(function (program) {
                return <DrawProgram
                    key={program.start + program.title}
                    program={program}
                />
            })}
        </ProgramContainer>
    </div>
}

const ChannelHeader = styled.div`
display: flex;
flex-direction: row;
padding: 0.5em;
align-items: center;
> * {
  margin-left: 0.5em;
}
`

const ChannelIcon = styled.img`
width: 2em;
height: 2em;
`
const ProgramContainer = styled.div`
max-height: 25em;
overflow: auto;
`
