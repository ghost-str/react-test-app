import {ChannelProgramInterface} from "../../api/channelProgramm";
import {dateToPrettyTimeFormat} from "../../utils/dateUtils";
import styled, {css, DefaultTheme} from "styled-components"
import * as React from "react";
import {ProgramType, ChannelProgramStateInterface} from "../../redux/channelsProgram/reduser";

interface DrawProgramProps {
    readonly program: ChannelProgramStateInterface
}

export function DrawProgram(props: DrawProgramProps) {
    // @ts-ignore
    return <StyledDiv color={props.program.type}>
        <div>
            {dateToPrettyTimeFormat(props.program.start)}
        </div>
        <div>
            {props.program.title}
        </div>
        <div>
            {props.program.ageLimit}+
        </div>
    </StyledDiv>
}


interface StyledPropsInterface {
    readonly  color: ProgramType
}

const StyledDiv = styled("div")<StyledPropsInterface>`
display: flex;
flex-direction: row;
justify-content: space-between;
margin-top: 0.05em;
padding: 0.25em;
> * {
  margin-left: 0.5em;
}
 ${(props) => {
    switch (props.color) {
        case ProgramType.past:
            return css`
                background-color: darkgray;
                   `
        case ProgramType.now:
            return css`
                 background-color: brown;   
                   `
        case ProgramType.future:
            return css`
                   background-color: unset;
                  `
    }
}}
`
