import * as React from "react";
import {connect} from "react-redux";
import {DrawChannel} from "./DrawChannel";
import styled from "styled-components";
import {ChannelStateInterface} from "../../redux/channelsProgram/reduser";
import {calcStatus} from "../../redux/channelsProgram/actions";

interface ChannelProgramProps {
    channels: Array<ChannelStateInterface>
    calcStatus: () => void
}


class DrawChannelsProgram extends React.Component<ChannelProgramProps, null> {
    intervalId: number = null

    componentDidMount(): void {
        this.intervalId = setInterval(() => {
            this.props.calcStatus()
        }, 3000)
    }

    render() {
        const {channels} = this.props
        return <ChannelsList>
            {channels.map(function (channel) {
                return <DrawChannel
                    key={channel.chid}
                    channel={channel}
                />
            })}
        </ChannelsList>
    }

    componentWillUnmount(): void {
        clearInterval(this.intervalId)
    }
}

export const ChannelProgram = connect(function (state) {
    return {
        // @ts-ignore
        channels: state.channels
    }
}, {calcStatus})(DrawChannelsProgram)

const ChannelsList = styled.div`
display: flex;
flex-direction: row;
flex-wrap: wrap;
justify-content: space-around;
> * {
  max-width: 20em;
  width: 100%;
}
`
