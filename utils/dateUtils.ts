import moment from 'moment'

export function parseApiDate(date: string): Date {
    return moment(date, 'YYYY-MM-DD HH:mm:ss').toDate()
}

export function toApiDate(date: Date): string {
    return moment(date).format('YYYY-MM-DD+HH:mm:ss')
}

export function dateToPrettyTimeFormat(date: Date): string {
    return moment(date).format('HH:mm')
}

export function addDateSeconds(date: Date, seconds: number): Date {
    return moment(date).add(seconds, "seconds").toDate()
}

export function dateIsBetween(date: Date, startDate: Date, endDate: Date) {
    return moment(date).isBetween(startDate, endDate)
}
