import request from "./request";
import {ChannelProgramInterface} from './channelProgramm'

export interface GetChannelsParams {
    domain: string
}

export interface ChannelInterface {
    chid: number,
    title: string,
    logo: string,
    description: string,
    url: string,
    hd: boolean,
    thid: number,
    display: number,
    xvid: number,
    button: number,
    bids: Array<number>,
    program: Array<ChannelProgramInterface>
}


export async function getChannels(params: GetChannelsParams): Promise<Array<ChannelInterface>> {
    const response = await request({
        url: '/channel/list',
        params
    })
    return parseChannels(response.data)
}

/**
 * @param channels
 */
function parseChannels(channels: Array<any>): Array<ChannelInterface> {
    return channels.map(function (channel) {
        return parseChannel(channel)
    })
}

/**
 * @param channel
 */
function parseChannel(channel): ChannelInterface {
    let bids = []
    if (channel.bids) {
        bids = channel.bids.split(',').map(function (item) {
            return +item
        })
    }
    return {
        chid: +channel.chid,
        title: channel.title,
        logo: process.env.BASE_HOST + channel.logo,
        description: channel.description,
        url: channel.url,
        hd: !!channel.hd,
        thid: +channel.thid,
        display: +channel.display,
        xvid: +channel.xvid,
        button: +channel.button,
        bids,
        program: []
    }
}
