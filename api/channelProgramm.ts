import request from "./request";
import {parseApiDate, toApiDate} from "../utils/dateUtils";

interface ChannelProgramParams {
    domain: string,
    dateFrom: Date,
    dateTo: Date,
    xvid: Array<number>
}

export async function getChannelProgram(requestParams: ChannelProgramParams) {
    const params = {
        'date_from': toApiDate(requestParams.dateFrom),
        'date_to': toApiDate(requestParams.dateTo),
        xvid: requestParams.xvid,
        domain: requestParams.domain
    }
    const response = await request({
        url: '/program/list',
        params
    })
    return parseChannelPrograms(response.data)
}

export interface ChannelProgramInterface {
    xvid: number,
    start: Date,
    duration: number,
    title: string,
    desc: string,
    icon: string,
    tid: number,
    ageLimit?: number
}

interface HashMap {
    [key: string]: Array<ChannelProgramInterface>
}

function parseChannelPrograms(programs: any): HashMap {
    const map: HashMap = {}
    Object.keys(programs).forEach(function (key) {
        map[key] = programs[key].map(function (program) {
            return parseChannelProgram(program)
        })
    })
    return map
}

const ageLimitRegExp = /^(.+)\[(\d+)\+\]$/u

function parseChannelProgram(program: any): ChannelProgramInterface {
    let ageLimit = null
    let title = program.title
    const parsed = ageLimitRegExp.exec(title)
    if (parsed) {
        ageLimit = parsed[2]
        title = parsed[1]
    }
    return {
        xvid: +program.xvid,
        start: parseApiDate(program.start),
        duration: +program.duration,
        title,
        ageLimit,
        desc: program.desc,
        icon: '/program/image?id=' + program.icon,
        tid: program.tid
    }
}
