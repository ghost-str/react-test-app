import axios from 'axios';
import qs from 'qs';

class HttpError extends Error {
    status?: number;
}

const request = axios.create({
        baseURL: process.env.BASE_HOST,
        timeout: 10000,
        paramsSerializer(params) {
            return qs.stringify(params, {encode: false})
        }
    }
);

export default request
