const result = require('dotenv').config()

if (result.error) {
  throw new Error('Ошибка чтения .env файла')
}

module.exports = {
  env: {
    BASE_URL: process.env.BASE_HOST,
  },
}
