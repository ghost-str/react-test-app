import {createStore, applyMiddleware} from 'redux'
import {composeWithDevTools} from 'redux-devtools-extension'
import reducer from './channelsProgram/reduser'
import thunk from "redux-thunk";

export const INIT_ACTION = '@@INIT'

export interface ActionInterface {
    type: string,
    payload?: any
}

export interface InitActionInterface extends ActionInterface {
    type: typeof INIT_ACTION
}

function initializeStore(preloadedState) {
    return createStore(
        reducer,
        preloadedState,
        composeWithDevTools(applyMiddleware(thunk))
    )
}

let reduxStore

export function getOrInitializeStore(initialState = null) {
    // Always make a new store if server, otherwise state is shared between requests
    if (typeof window === 'undefined') {
        return initializeStore(initialState)
    }

    // Create store if unavailable on the client and set it on the window object
    if (!reduxStore) {
        reduxStore = initializeStore(initialState)
    }

    return reduxStore
}

