export default function (action: Function) {
    return function (...args) {
        return function (dispatch, getState) {
            action({ dispatch, getState }, ...args)
        }
    }
}
