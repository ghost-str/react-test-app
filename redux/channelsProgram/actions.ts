import {getChannels} from "../../api/channels";
import {getChannelProgram} from "../../api/channelProgramm";
import actionWrapper from "../actionWrapper";
import {
    FETCH_CHANNELS_WITH_PROGRAM_REJECTED,
    FETCH_CHANNELS_WITH_PROGRAM_FULFILLED,
    FETCH_CHANNELS_WITH_PROGRAM_PENDING,
    CALC_CHANNEL_PROGRAM_STATUS,
    ChannelsProgramActionsTypes
} from './types'
import {Store} from "redux";
import {ChannelsProgramStateInterface} from "./reduser";


interface getChannelsWithProgramProps {
    dateFrom: Date,
    dateTo: Date,
    domain: string
}


export const fetchChannelsWithProgram = async function ({dispatch}: Store<ChannelsProgramStateInterface, ChannelsProgramActionsTypes>, props: getChannelsWithProgramProps) {
    dispatch({
        type: FETCH_CHANNELS_WITH_PROGRAM_PENDING
    })
    try {
        const channels = await getChannels({domain: props.domain})
        const xvidList = channels.map(function (channel) {
            return channel.xvid
        })
        const channelPrograms = await getChannelProgram({
            domain: props.domain,
            dateFrom: props.dateFrom,
            dateTo: props.dateTo,
            xvid: xvidList
        })
        Object.keys(channelPrograms).forEach(function (key) {
            const channel = channels.find(function (channel) {
                return channel.xvid === +key
            })
            channel.program = channelPrograms[key]
        })
        dispatch({
            type: FETCH_CHANNELS_WITH_PROGRAM_FULFILLED,
            payload: channels
        })
    } catch (e) {
        dispatch({
            type: FETCH_CHANNELS_WITH_PROGRAM_REJECTED,
            payload: e
        })
    }
}
export const calcStatus = actionWrapper(function ({dispatch}) {
    dispatch({type: CALC_CHANNEL_PROGRAM_STATUS})
})

