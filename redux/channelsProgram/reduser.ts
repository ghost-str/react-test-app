import {ChannelInterface} from "../../api/channels";
import {
    ChannelsProgramActionsTypes,
    FETCH_CHANNELS_WITH_PROGRAM_PENDING,
    FETCH_CHANNELS_WITH_PROGRAM_FULFILLED,
    FETCH_CHANNELS_WITH_PROGRAM_REJECTED,
    CALC_CHANNEL_PROGRAM_STATUS
} from "./types";
import {INIT_ACTION} from "../Store";
import {ChannelProgramInterface} from "../../api/channelProgramm";
import {addDateSeconds, dateIsBetween} from "../../utils/dateUtils";

export enum ProgramType {
    past,
    now,
    future
}

export interface ChannelProgramStateInterface extends ChannelProgramInterface {
    type: ProgramType
}

export interface ChannelStateInterface extends ChannelInterface {
    program: Array<ChannelProgramStateInterface>
}


export interface ChannelsProgramStateInterface {
    channels: Array<ChannelStateInterface>
    isLoading: boolean
    error?: Error
}

export default function (state: ChannelsProgramStateInterface, action: ChannelsProgramActionsTypes): ChannelsProgramStateInterface {
    if (state == null) {
        return {
            channels: [],
            isLoading: false,
            error: null
        }
    }

    switch (action.type) {
        case CALC_CHANNEL_PROGRAM_STATUS:
            return calcProgramStatus(state)
        case FETCH_CHANNELS_WITH_PROGRAM_PENDING:
            return {
                ...state,
                isLoading: true
            }
        case FETCH_CHANNELS_WITH_PROGRAM_FULFILLED:
            return calcProgramStatus({
                channels: action.payload,
                isLoading: false
            })
        case FETCH_CHANNELS_WITH_PROGRAM_REJECTED:
            return {
                ...state,
                isLoading: false,
                error: action.payload
            }
        case INIT_ACTION:
            // возвращаем в исходное состояние
            return initSate(state)
        default:
            return state
    }
}

function initSate(state: ChannelsProgramStateInterface): ChannelsProgramStateInterface {
    return {
        isLoading: state.isLoading,
        error: state.error,
        channels: state.channels.map(function (item) {
            return {
                ...item,
                program: item.program.map(function (program) {
                    return {
                        ...program,
                        start: new Date(program.start)
                    }
                })
            }
        })
    }
}

function calcProgramStatus(state: ChannelsProgramStateInterface): ChannelsProgramStateInterface {
    return {
        ...state,
        channels: state.channels.map(function (channel) {
            return {
                ...channel,
                program: channel.program.map(function (program) {
                    return {
                        ...program,
                        type: calculateProgramType(program)
                    }
                })
            }
        })
    }
}

function calculateProgramType(program: ChannelProgramStateInterface): ProgramType {
    const now = new Date()
    const endDate = new Date(program.start)
    endDate.setSeconds(endDate.getSeconds() + program.duration)
    if (now > program.start && now < endDate) {
        return ProgramType.now
    }
    if (now < endDate) {
        return ProgramType.future
    }
    return ProgramType.past
}
