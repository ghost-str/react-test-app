import {ActionInterface} from "../Store";
import {PENDING, FULFILLED, REJECTED} from "../../enum/actionStatusPrefix";
import {ChannelInterface} from "../../api/channels";
import {InitActionInterface} from "../Store";

export const FETCH_CHANNELS_WITH_PROGRAM = 'GET_CHANNELS_WITH_PROGRAM'
export const FETCH_CHANNELS_WITH_PROGRAM_PENDING = FETCH_CHANNELS_WITH_PROGRAM + PENDING
export const FETCH_CHANNELS_WITH_PROGRAM_FULFILLED = FETCH_CHANNELS_WITH_PROGRAM + FULFILLED
export const FETCH_CHANNELS_WITH_PROGRAM_REJECTED = FETCH_CHANNELS_WITH_PROGRAM + REJECTED
export const CALC_CHANNEL_PROGRAM_STATUS = 'CALC_CHANNEL_PROGRAM_STATUS'

export interface CalcChannelProgramStatusInterface extends ActionInterface {
    type: typeof CALC_CHANNEL_PROGRAM_STATUS
}

export interface ChannelWithProgramPendingInterface extends ActionInterface {
    type: typeof FETCH_CHANNELS_WITH_PROGRAM_PENDING
}

export interface FetchChannelsWithProgramFullFiledInterface extends ActionInterface {
    type: typeof FETCH_CHANNELS_WITH_PROGRAM_FULFILLED
    payload: Array<ChannelInterface>
}

export interface FetchChannelsWithProgramErrorInterface extends ActionInterface {
    type: typeof FETCH_CHANNELS_WITH_PROGRAM_REJECTED
    payload: Error
}

export type ChannelsProgramActionsTypes =
    ChannelWithProgramPendingInterface
    | FetchChannelsWithProgramFullFiledInterface
    | FetchChannelsWithProgramErrorInterface
    | InitActionInterface
    | CalcChannelProgramStatusInterface
